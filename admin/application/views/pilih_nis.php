<div class="container-fluid">
  <!-- Page Heading -->

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Pilih User</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">

	<form class="form-horizontal" method="post" action="create">
		<div class="modal-body">
    	<table class="table table-bordered" id="dataTable"cellspacing="0">
		  	<thead>
				<tr>
				 <th>Pilih</th>
				 <th>NIS</th>
				 <th>Nama</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($rows as $row){?>
				<tr>
					<td><input type="submit" class="btn btn-success" name="pilih" value="Pilih"/></td>
					<td><?php echo $row->nis; ?></td>
					<td><?php echo $row->nama_alumni; ?></td>
					<input type="text" name="nis" value="<?php echo $row->nis; ?>" hidden/>
				</tr>
				<?php } ?>
			</tbody>
		</table>
        </div>
	</form>
</div>
</div>
</div>
</div>