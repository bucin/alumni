
	<!-- breadcrumb start-->
	<section class="breadcrumb breadcrumb_bg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="breadcrumb_iner text-center">
						<div class="breadcrumb_iner_item">
							<h2>Tentang Kami</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
    <!-- breadcrumb start-->

    <!-- feature part start-->
    <section class="feature_part padding_top">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-7">
                    <div class="feature_img">
                        <img src="<?php echo base_url('assets/img/man.jpg');?>" alt="">
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="feature_part_text">
                        <img src="<?php echo base_url('assets/img/gdg.jpg');?>" alt="#">
                        <h2>Tentang Sekolah</h2>
                        <p>SMA Negeri 1 Jember berdiri tahun 1953. Gedung tua ini menjadi saksi sejarah berdrinya sebuah lembaga pendidikan SMA Negeri pertama di Kabupaten Jember, yang didirikan dengan semangat gotong royong oleh masyarakat Jember. SMA Negeri 1 Jember dengan segudang prestasi yang diraih pada saat ini telah mampu mensejajarkan diri dengan SMA terbaik di negeri ini dan dikembangkan oleh pemerintah menjadi Sekolah bertaraf Internasional (SBI), SMA Negeri 1 adalah salah satunya.

                        </p>
                        <span>Dalam perjalanannya yang sudah lebih dari setengah abad, SMA Negeri 1 Jember selalu berada di hati masyarakat Jember karena mutu pendidikan yang baik dan prestasi yang membangggakan.
                            ut</span>
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <div class="feature_part_text_iner">
                                    <img src="<?php echo base_url('assets/img/student1.png');?>" alt="">
                                    <h4>Murid</h4>
                                    <p>1000 murid</p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <div class="feature_part_text_iner">
                                    <img src="<?php echo base_url('assets/img//class.png');?>" alt="">
                                    <h4>Ruang</h4>
                                    <p>75 ruang</p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <div class="feature_part_text_iner">
                                    <img src="<?php echo base_url('assets/img/guru.png');?>" alt="">
                                    <h4>Guru</h4>
                                    <p>85 guru/p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img src="<?php echo base_url('assets/img/animate_icon/Shape-1.png');?>" alt="" class="feature_icon_1">
        <img src="<?php echo base_url('assets/img/animate_icon/Shape-2.png');?>" alt="" class="feature_icon_2">
        <img src="<?php echo base_url('assets/img/animate_icon/Shape-3.png');?>" alt="" class="feature_icon_3">
    </section>
    <!-- feature part end-->

    <!-- popular place part start-->
    <section class="popular_place padding_top">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5">
                    <div class="section_tittle text-center">
                        <img src="<?php echo base_url('assets/img/section_tittle_img.png');?>" alt="">
                        <h2>Fasilitas <span>sekolah</span> </h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit 
                        sed  do eiusmod tempor incididunt ut</p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-4 col-sm-6">
                    <div class="single_popular_place">
                        <img src="<?php echo base_url('assets/img/icon/place_icon_1.png');?>" alt="">
                        <h4>Perpustkaan</h4>
                        <p>Perpustakaan yang luas dan suasana tenang dengan koleksi buku yang lengkap </p>
                       
                    </div>
                </div><div class="col-lg-4 col-sm-6">
                    <div class="single_popular_place">
                        <img src="<?php echo base_url('assets/img/icon/place_icon_2.png');?>" alt="">
                        <h4>Ruang Kelas</h4>
                        <p>Ruang kelas pilihan dengan fasilitas yang lengkap untuk belajar</p>
                        
                    </div>
                </div><div class="col-lg-4 col-sm-6">
                    <div class="single_popular_place">
                        <img src="<?php echo base_url('assets/img/icon/place_icon_3.png');?>" alt="">
                        <h4>Laboratorium</h4>
                        <p>Perlengkapan laboratorium yang lengkap untuk mempraktikkan teori yang dipelajari
                    </p>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- popular place part end-->

    <!-- blog post part start-->

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- blog post part end-->