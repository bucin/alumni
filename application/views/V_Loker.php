
    <!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center" style="height: 60px;">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- breadcrumb start-->

 <!-- use sasu part end-->
    <section class="popular_place padding_top" style="padding-top: 5%;">
        <div class="container">
            
            <div class="row justify-content-center">
            <?php foreach($data as $a){
                //Limit text Strength
                $string = $a->isi_event;
                if (strlen($string) > 90) {
                   $stringCut = substr($string, 0, 90);
                   $endPoint = strrpos($stringCut, ' ');
                   $string = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
                   $string .= '...';
                }
            ?>
                <div class="col-lg-4 col-sm-6" style="margin-bottom: 20px;">
                    <div class="single_popular_place">
                        <img src="<?php echo base_url('assets/img/'.$a->gambar_event);?>" alt="">
                        <h4><?php echo $a->judul_event; ?></h4>
                        <p><?php echo $string; ?></p>
                        <a href="<?php echo base_url('Loker/detail/'.$a->id_event); ?>" class="btn1">Kunjungi</a>
                    </div>
                </div>
            <?php } ?>
            </div>
        </div>
    </section>
    <!-- use sasu part end-->

    <nav class="blog-pagination justify-content-center d-flex">
                            <ul class="pagination">
                                <li class="page-item">
                                    <a href="#" class="page-link" aria-label="Previous">
                                        <i class="ti-angle-left"></i>
                                    </a>
                                </li>
                                <li class="page-item">
                                    <a href="#" class="page-link">1</a>
                                </li>
                                <li class="page-item active">
                                    <a href="#" class="page-link">2</a>
                                </li>
                                <li class="page-item">
                                    <a href="#" class="page-link" aria-label="Next">
                                        <i class="ti-angle-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </nav>