<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index(){
		$this->load->view('widget/header');
		$this->load->view('Welcome_message');
		$this->load->view('widget/footer');
	}
}
