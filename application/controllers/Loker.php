<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loker extends CI_Controller {

	function __construct(){
		parent::__construct();	
		$this->load->model('M_Loker');	
	}

	public function index(){
		$data['data'] = $this->M_Loker->tampil_loker();
		$this->load->view('widget/header');
		$this->load->view('V_Loker',$data);
		$this->load->view('widget/footer');
	}

	public function detail($id){
		$data['data'] = $this->M_Loker->detail_loker($id);
		$this->load->view('widget/header');
		$this->load->view('V_LokerDetail',$data);
		$this->load->view('widget/footer');
	}
}
