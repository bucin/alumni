<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	public function index(){
		$this->load->view('widget/header');
		$this->load->view('V_Contact');
		$this->load->view('widget/footer');
	}
}
