<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataAlumni extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_Alumni');		
		$this->load->helper(array('url'));
		if($this->session->userdata('status') != "login"){
			echo "<script>
                alert('Anda harus login terlebih dahulu');
                window.location.href = '".base_url('Login')."';
            </script>";//Url tujuan
		}
	}

	public function index(){
		$data['data'] = $this->M_Alumni->tampil_alumni();
		$this->load->view('widget/header');
		$this->load->view('V_Alumni', $data);
		$this->load->view('widget/footer');
	}

  public function search(){
    $keyword = $_GET["keyword"];
    $no=1;
    if(empty($this->M_Alumni->search($keyword))){
      echo "
          <div class='container alert alert-warning'>
            <strong>Mohon maaf,</strong> data yang anda cari tidak ditemukan!.
          </div>
      ";
    }else {
      foreach($this->M_Alumni->search($keyword) as $a){
        echo "
              <div class='table-row'>
                <div class='serial'>".$no++."</div>
                <div class='country'>".$a->nama_alumni."</div>
                <div class='visit'>".$a->tahun_masuk."</div>
                <div class='visit'>".$a->alamat_skr."</div>
                <div class='percentage'><a href='#''>Detail &nbsp<i class='fas fa-external-link-alt'></i></a></div>
              </div>
        ";
      }
    }
  }
}

