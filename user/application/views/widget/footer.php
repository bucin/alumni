
	<!--::footer_part start::-->
    <footer class="footer_part">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-lg-3">
                    <div class="single_footer_part">
                        <h4>Tentang</h4>
                        <p>Jl. Tugu Utara No.1, Klojen
Kota Malang, Jawa Timur 65111

(0341) 366454

mitrekasatata@sman1-mlg.sch.id

sman1-mlg.sch.id</p>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="single_footer_part">
                        <h4>Navigasi</h4>
                        <ul class="list-unstyled">
                            <li><a href="#">Beranda</a></li>
                            <li><a href="#">Alumni</a></li>
                            <li><a href="#">Acara</a></li>
                            <li><a href="#">Tentang kami</a></li>
                            <li><a href="#">Kontak</a></li>
                            <li><a href="#">Profil Saya</a></li>
                          >
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="single_footer_part">
                        <h4>Email</h4>
                        <p>Untuk pertanyaat lebih lanjut bisa memasukkan email Anda dibawah ini 
                        </p>
                        <div id="mc_embed_signup">
                            <form target="_blank"
                                action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscribe_form relative mail_part">
                                <input type="email" name="email" id="newsletter-form-email" placeholder="Email Address"
                                    class="placeholder hide-on-focus" onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = ' Email Address '">
                                <button type="submit" name="submit" id="newsletter-submit"
                                    class="email_icon newsletter-submit button-contactForm"><i
                                        class="far fa-paper-plane"></i></button>
                                <div class="mt-10 info"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="single_footer_part">
                        <h4>Instafeed</h4>
                        <div class="footer_img">
                            <a href="#"><img src="<?php echo base_url('assets/img/footer_img/footer_1.png'); ?>" alt=""></a>
                            <a href="#"><img src="<?php echo base_url('assets/img/footer_img/footer_2.png');?>" alt=""></a>
                            <a href="#"><img src="<?php echo base_url('assets/img/footer_img/footer_3.png');?>" alt=""></a>
                            <a href="#"><img src="<?php echo base_url('assets/img/footer_img/footer_4.png');?>" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-lg-8">
                    <div class="copyright_text">
                        <P><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></P>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="footer_icon social_icon">
                        <ul class="list-unstyled">
                            <li><a href="#" class="single_social_icon"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#" class="single_social_icon"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#" class="single_social_icon"><i class="fas fa-globe"></i></a></li>
                            <li><a href="#" class="single_social_icon"><i class="fab fa-behance"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <img src="img/overlay_2.png" alt="#" class="footer_overlay">
    </footer>
    <!--::footer_part end::-->

    <!-- jquery plugins here-->
    <!-- jquery -->
    <!-- <script src="<?php echo base_url('assets/jquery.min.js'); ?>"></script> -->
    <script src="<?php echo base_url('assets/js/jquery-1.12.1.min.js'); ?>"></script>
    <!-- popper js -->
    <script src="<?php echo base_url('assets/js/popper.min.js');?>"></script>
    <!-- bootstrap js -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
    <!-- easing js -->
    <script src="<?php echo base_url('assets/js/jquery.magnific-popup.js');?>"></script>
    
    
    
    
    <!-- particles js -->
    <script src="<?php echo base_url('assets/js/owl.carousel.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.nice-select.min.js');?>"></script>
    <!-- slick js -->
    <script src="<?php echo base_url('assets/js/slick.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.counterup.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/waypoints.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/contact.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.ajaxchimp.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.form.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.validate.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/mail-script.js');?>"></script>
    <!-- custom js -->
    <script src="<?php echo base_url('assets/js/custom.js');?>"></script>
    <script src="<?php echo base_url('assets/ckeditor/ckeditor.js');?>"></script>
</body>

</html>