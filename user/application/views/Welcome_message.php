
<section class="banner_part">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7">
                    <div class="banner_text">
                        <div class="banner_text_iner">
                            <h1>Alumniku</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner part start-->

    <!-- banner part start-->
    <section class="search_your_country">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="search_form">
                        <form action="#">
                            <div class="form-row">
                                <div class="col-lg-12">
                                   <center> <h1>Sistem Informasi Manajemen Alumni</h1>
                                    <h3 style="color: grey">Sistem yang digunakan untuk pendataan Alumni</h3></center>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
           

     <!-- feature part start-->
    <section class="feature_part padding_top">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-7">
                    <div class="feature_img">
                        <img src="<?php echo base_url('assets/img/man.jpg');?>" alt="">
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="feature_part_text">
                        <img src="<?php echo base_url('assets/img/gdg.jpg');?>" alt="#">
                        <h2>Tentang Sekolah</h2>
                        <p>SMA Negeri 1 Jember berdiri tahun 1953. Gedung tua ini menjadi saksi sejarah berdrinya sebuah lembaga pendidikan SMA Negeri pertama di Kabupaten Jember, yang didirikan dengan semangat gotong royong oleh masyarakat Jember. SMA Negeri 1 Jember dengan segudang prestasi yang diraih pada saat ini telah mampu mensejajarkan diri dengan SMA terbaik di negeri ini dan dikembangkan oleh pemerintah menjadi Sekolah bertaraf Internasional (SBI), SMA Negeri 1 adalah salah satunya.

                        </p>
                        <span>Dalam perjalanannya yang sudah lebih dari setengah abad, SMA Negeri 1 Jember selalu berada di hati masyarakat Jember karena mutu pendidikan yang baik dan prestasi yang membangggakan.
                            ut</span>
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <div class="feature_part_text_iner">
                                    <img src="<?php echo base_url('assets/img/student1.png');?>" alt="">
                                    <h4>Murid</h4>
                                    <p>1000 murid</p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <div class="feature_part_text_iner">
                                    <img src="<?php echo base_url('assets/img//class.png');?>" alt="">
                                    <h4>Ruang</h4>
                                    <p>75 ruang</p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <div class="feature_part_text_iner">
                                    <img src="<?php echo base_url('assets/img/guru.png');?>" alt="">
                                    <h4>Guru</h4>
                                    <p>85 guru/p>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <img src="<?php echo base_url('assets/img/animate_icon/Shape-1.png');?>" alt="" class="feature_icon_1">
        <img src="<?php echo base_url('assets/img/animate_icon/Shape-2.png');?>" alt="" class="feature_icon_2">
        <img src="<?php echo base_url('assets/img/animate_icon/Shape-3.png');?>" alt="" class="feature_icon_3">
    </section>
    <!-- upcoming_event part start-->

    <!-- use sasu part end-->
    <section class="popular_place padding_top">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5">
                    <div class="section_tittle text-center">
                        <img src="<?php echo base_url('assets/img/section_tittle_img.png');?>" alt="">
                        <h2>Sekilas Berita </h2>
                     
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-4 col-sm-6">
                    <div class="single_popular_place">
                        <img src="<?php echo base_url('assets/img/icon/place_icon_1.png');?>" alt="">
                        <h4>Life of Egeft</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor</p>
                        <a href="#" class="btn1">book now</a>
                    </div>
                </div><div class="col-lg-4 col-sm-6">
                    <div class="single_popular_place">
                        <img src="<?php echo base_url('assets/img/icon/place_icon_2.png');?>" alt="">
                        <h4>Biking in Norway</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor</p>
                        <a href="#" class="btn1">book now</a>
                    </div>
                </div><div class="col-lg-4 col-sm-6">
                    <div class="single_popular_place">
                        <img src="<?php echo base_url('assets/img/icon/place_icon_3.png');?>" alt="">
                        <h4>Tour of iceland</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor</p>
                        <a href="#" class="btn1">book now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- use sasu part end-->

    <!-- about_us part start-->
    <section class="place_details section_padding">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <div class="col-md-6 col-lg-6">
                    <div class="place_detauls_text">
                        <div class="row justify-content-center">
                            <div class="col-lg-10 col-xl-6">
                                <div class="place_details_content">
                                    <img src="<?php echo base_url('assets/img/section_tittle_img.png');?>" alt="#">
                                    <h2>SMA Negeri 1 Malang</h2>
                                    <p>SMA Negeri 1 Malang, adalah Sekolah Menengah Atas Negeri, yang terletak di jalan Tugu Utara No. 1, Malang, Jawa Timur, Indonesia. Sekolah ini terletak di dalam satu kompleks dengan Stasiun Malang yang dikenal dengan sebutan SMA Tugu bersama-sama dengan SMA Negeri 3 Malang dan SMA Negeri 4 Malang. mereka dikenal dengan julukan SMA Tugu, dikarenakan terletak di jalan Tugu yang terkenal di Malang. </p>
                                    
                                </div>
                            </div>
                        </div>
                        <img src="<?php echo base_url('assets/img/perpus.jpg');?>" alt="#">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="place_details_img">
                        <img src="<?php echo base_url('assets/img/plase_details_2.png');?>" alt="#">
                    </div>
                </div>
            </div>
        </div>
        <div class="view_all_btn">
            <a href="#" class="view_btn">view all</a>
        </div>
    </section>
    <!-- about_us part end-->

    <!-- pricing part start-->
    <section class="tour_package section_padding">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-lg-6 col-sm-6">
                    <div class="tour_package_cotent owl-carousel">
                        <div class="single_tour_package">
                            <img src="<?php echo base_url('assets/img/tour_plan_1.png');?>" alt="">
                            <div class="tour_pack_content">
                                <h4>Sawpalo, Brasil</h4>
                                <p> Lorem ipsum dolor sit amet, consectetur adipi
                                    elit sed do eiusmod tempor incididunt</p>
                                <div class="tour_content_rating">
                                    <ul>
                                        <li><a href="#"><i class="fas fa-star"></i></a></li>
                                        <li><a href="#"><i class="fas fa-star"></i></a></li>
                                        <li><a href="#"><i class="fas fa-star"></i></a></li>
                                        <li><a href="#"><i class="fas fa-star"></i></a></li>
                                        <li><a href="#"><i class="fas fa-star"></i></a></li>
                                    </ul>
                                    <a href="#" class="btn1">join now</a>
                                </div>
                            </div>
                        </div>
                        <div class="single_tour_package">
                            <img src="<?php echo base_url('assets/img/tour_plan_2.png');?>" alt="">
                            <div class="tour_pack_content">
                                <h4>Sawpalo, Brasil</h4>
                                <p> Lorem ipsum dolor sit amet, consectetur adipi
                                    elit sed do eiusmod tempor incididunt</p>
                                <div class="tour_content_rating">
                                    <ul>
                                        <li><a href="#"><i class="fas fa-star"></i></a></li>
                                        <li><a href="#"><i class="fas fa-star"></i></a></li>
                                        <li><a href="#"><i class="fas fa-star"></i></a></li>
                                        <li><a href="#"><i class="fas fa-star"></i></a></li>
                                        <li><a href="#"><i class="fas fa-star"></i></a></li>
                                    </ul>
                                    <a href="#" class="btn1">join now</a>
                                </div>
                            </div>
                        </div>
                        <div class="single_tour_package">
                            <img src="<?php echo base_url('assets/img/tour_plan_1.png');?>" alt="">
                            <div class="tour_pack_content">
                                <h4>Sawpalo, Brasil</h4>
                                <p> Lorem ipsum dolor sit amet, consectetur adipi
                                    elit sed do eiusmod tempor incididunt</p>
                                <div class="tour_content_rating">
                                    <ul>
                                        <li><a href="#"><i class="fas fa-star"></i></a></li>
                                        <li><a href="#"><i class="fas fa-star"></i></a></li>
                                        <li><a href="#"><i class="fas fa-star"></i></a></li>
                                        <li><a href="#"><i class="fas fa-star"></i></a></li>
                                        <li><a href="#"><i class="fas fa-star"></i></a></li>
                                    </ul>
                                    <a href="#" class="btn1">join now</a>
                                </div>
                            </div>
                        </div>
                        <div class="single_tour_package">
                            <img src="<?php echo base_url('assets/img/tour_plan_2.png'); ?>" alt="">
                            <div class="tour_pack_content">
                                <h4>Sawpalo, Brasil</h4>
                                <p> Lorem ipsum dolor sit amet, consectetur adipi
                                    elit sed do eiusmod tempor incididunt</p>
                                <div class="tour_content_rating">
                                    <ul>
                                        <li><a href="#"><i class="fas fa-star"></i></a></li>
                                        <li><a href="#"><i class="fas fa-star"></i></a></li>
                                        <li><a href="#"><i class="fas fa-star"></i></a></li>
                                        <li><a href="#"><i class="fas fa-star"></i></a></li>
                                        <li><a href="#"><i class="fas fa-star"></i></a></li>
                                    </ul>
                                    <a href="#" class="btn1">join now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-xl-3 offset-lg-1 col-sm-6">
                    <div class="tour_pack_content">
                        <img src="<?php echo base_url('assets/img/section_tittle_img.png'); ?>" alt="">
                        <h2>tour package</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit 
                        sed  do eiusmod tempor incididunt ut</p>
                        <a href="#" class="btn_1">join now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- pricing part end-->

    <!-- cta part start-->
    <section class="cta_part section_padding">
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-8 col-xl-7">
                    <div class="cta_text text-center">
                        <h5>Join Our Newsletter</h5>
                        <h2>Subscribe to get Updated
                                with new offers</h2>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Enter email address" aria-label="Recipient's username with two button addons" aria-describedby="button-addon4">
                            <div class="input-group-append" id="button-addon4">
                                <a href="#" class="subs_btn">subscribe now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- cta part end-->

    <!-- blog part start-->
    <section class="blog_part padding_top">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="section_tittle">
                        <img src="<?php echo base_url('assets/img/section_tittle_img.png');?>" alt="">
                        <h2>our blog</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit 
                        sed  do eiusmod tempor incididunt ut</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="single_blog_part">
                        <img src="<?php echo base_url('assets/img/blog/blog_1.png');?>" alt="">
                        <div class="blog_text">
                            <h2>Luxerious Car Rental</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                            sed do eiusmod tempor incididunt ut labore et dolore magna 
                            aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                            <ul>
                                <li>
                                    <i class="ti-calendar"></i>
                                    <p>13th Dec</p>
                                </li>
                                <li>
                                    <i class="ti-heart"></i>
                                    <p>15</p>
                                </li>
                                <li>
                                    <i class="far fa-comment-dots"></i>
                                    <p>10</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="single_blog_part">
                        <img src="<?php echo base_url('assets/img/blog/blog_2.png');?>" alt="">
                        <div class="blog_text">
                            <h2>Luxerious Car Rental</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                            sed do eiusmod tempor incididunt ut labore et dolore magna 
                            aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                            <ul>
                                <li>
                                    <i class="ti-calendar"></i>
                                    <p>13th Dec</p>
                                </li>
                                <li>
                                    <i class="ti-heart"></i>
                                    <p>15</p>
                                </li>
                                <li>
                                    <i class="far fa-comment-dots"></i>
                                    <p>10</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="single_blog_part">
                        <img src="<?php echo base_url('assets/img/blog/blog_3.png');?>" alt="">
                        <div class="blog_text">
                            <h2>Luxerious Car Rental</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                            sed do eiusmod tempor incididunt ut labore et dolore magna 
                            aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                            <ul>
                                <li>
                                    <i class="ti-calendar"></i>
                                    <p>13th Dec</p>
                                </li>
                                <li>
                                    <i class="ti-heart"></i>
                                    <p>15</p>
                                </li>
                                <li>
                                    <i class="far fa-comment-dots"></i>
                                    <p>10</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img src="<?php echo base_url('assets/img/overlay_1.png');?>" alt="#" class="blog_img">
    </section>
    <!-- blog part end-->
