
    <!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center" style="height: 60px;">
                    </div>
                </div>
            </div>
        </div>
    </section> <br>
    <!-- breadcrumb start-->

<div class="container"> 
<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Tambah Artikel</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
          <artikel>
              <?php echo form_open_multipart('Upload_prestasi/create');?>
                  <table width="100%" align="center" cellspacing="2" cellpadding="10">
                    <tr>
                        <td ><b >Tahun Prestasi</b></td>
                        <td><input type="number" style="width: 20%;" class="form-control" id="thn_prestasi" name="thn_prestasi" required></td>
                    </tr>
                    <tr>
                        <td ><b>Prestasi</b></td>
                        <td><input type="text" style="width: 50%;" class="form-control" id="prestasi" name="prestasi" id="prestasi"></textarea></td>
                    </tr>   
                   <tr>
                        <td ><b>Tingkat Prestasi</b></td>
                        <td>
                            <select class="form-control" name="tingkat_prestasi">
                                <option value="Desa">Desa</option>
                                <option value="Kecamatan">Kecamatan</option>
                                <option value="Kabupaten">Kabupaten</option>
                                <option value="Provinsi">Provinsi</option>
                                <option value="Nasional">Nasional</option>
                                <option value="Internasional">Internasional</option>
                            </select>
                            <!-- <input type="text"class="form-control" name="tingkat_prestasi" /> -->
                        </td>
                    </tr>
                    <tr>
                        <td ><b>Juara Prestasi</b></td>
                        <td><input type="text" style="width: 50%;" class="form-control" name="juara_prestasi"/></td>
                    </tr>
                                         <tr>
                        <td ><b>Gambar Prestasi</b></td>
                        <td><input type="file" name="gambar_prestasi" ></td>
                    </tr>

                </table>
                <br>
                <center>
                    <input type="button" class="genric-btn danger radius" name="Batal" value="BATAL" onclick="javascript:history.go(-1);"/>

                    <input type="submit" class="genric-btn primary radius" name="btnSubmit" value="KIRIM">
                </center>
              <?php echo form_close(); ?>
        </artikel><br><br>
    </div>
    </div>
</div>
</div>  